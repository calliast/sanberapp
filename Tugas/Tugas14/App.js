import React from 'react';
import Main from './components/Main';

export default class App extends React.Component {
  render() {
    return (
      <Main />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
      width: 200,
      height: 200,
      backgroundColor: 'skyblue',
      borderWidth: 2,
      borderColor: 'steelblue',
      borderRadius: 20,
  }
});